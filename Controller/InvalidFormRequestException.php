<?php

declare(strict_types=1);

namespace Makukh\PersonalDiscount\Controller;

use InvalidArgumentException;

class InvalidFormRequestException extends InvalidArgumentException
{

}
