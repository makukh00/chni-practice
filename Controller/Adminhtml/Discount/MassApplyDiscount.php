<?php
declare(strict_types=1);

namespace Makukh\PersonalDiscount\Controller\Adminhtml\Discount;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Store\Model\StoreManagerInterface;
use Makukh\PersonalDiscount\Model\ResourceModel\DiscountRequest\CollectionFactory as DiscountRequestCollectionFactory;

class MassApplyDiscount extends AbstractMassAction
{

    /**
     * @var \Magento\SalesRule\Model\RuleFactory
     */
    private $ruleFactory;
    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    private $customerRepository;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    private $productRepository;

    public function __construct(
        \Magento\Ui\Component\MassAction\Filter           $filter,
        DiscountRequestCollectionFactory                  $discountRequestCollectionFactory,
        \Magento\Framework\DB\TransactionFactory          $transactionFactory,
        \Magento\Backend\App\Action\Context               $context,
        \Magento\SalesRule\Model\RuleFactory              $ruleFactory,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        StoreManagerInterface                             $storeManager,
        \Magento\Catalog\Api\ProductRepositoryInterface   $productRepository
    )
    {
        parent::__construct($filter, $discountRequestCollectionFactory, $transactionFactory, $context);

        $this->ruleFactory = $ruleFactory;
        $this->customerRepository = $customerRepository;
        $this->storeManager = $storeManager;
        $this->productRepository = $productRepository;
    }

    /**
     * @inheritDoc
     * @throws LocalizedException
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        // if not Approved, redirect
//        if ($status !== 2) {
//            return $resultRedirect->setPath('*/*/');
//        }

        $collection = $this->filter->getCollection($this->discountRequestCollectionFactory->create());
        $collectionSize = $collection->count();

        foreach ($collection as $item) {
            if ((int)$item->getStatus() !== 2) {
                $this->messageManager->addErrorMessage(__('Can\'t apply discount for this item with id: %1 ', $item->getId()));
                return $resultRedirect->setPath('*/*/');
            }
            $this->applyDiscount($item->getCustomerId(), $item->getProductId(), $item->discouunt);

        }


        $this->messageManager->addSuccessMessage(__('%1 requests(s) have been deleted.', $collectionSize));
        return $resultRedirect->setPath('*/*/');
    }


    public function applyDiscount(int $customerId, int $productId, int $discountAmount)
    {
        $customer = $this->getCustomerById($customerId);
        $product = $this->getProductById($productId);

        try {


            // Create a new rule
            $rule = $this->ruleFactory->create();
            $rule->setName('Specific Product Discount')
                ->setDescription('Discount for product for a specific customer')
                ->setIsActive(1)
                ->setCustomerGroupIds((string)[$customer->getGroupId()])
                ->setCouponType(\Magento\SalesRule\Model\Rule::COUPON_TYPE_NO_COUPON)
                ->setUsesPerCustomer(1)
                ->setDiscountAmount($discountAmount)
                ->setDiscountQty(1)
                ->setSimpleAction(\Magento\SalesRule\Model\Rule::BY_PERCENT_ACTION)
                ->setStopRulesProcessing(0)
                ->setWebsiteIds((string)$this->storeManager->getWebsite());

            $conditions = [
                'type' => \Magento\SalesRule\Model\Rule\Condition\Product\Found::class,
                'value' => 1,
                'aggregator' => 'all',
                'conditions' => [[
                    'type' => \Magento\SalesRule\Model\Rule\Condition\Product::class,
                    'attribute' => 'sku',
                    'operator' => '==',
                    'value' => $product->getSku(),
                ]],
            ];

            $rule->getConditions()->loadArray($conditions);
            $rule->save();
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * @param int $customerId
     * @return \Magento\Customer\Api\Data\CustomerInterface|null
     * @throws LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getCustomerById(int $customerId)
    {
        try {
            return $this->customerRepository->getById($customerId);
        } catch (\Magento\Framework\Exception\NoSuchEntityException) {
            return null;
        }
    }

    /**
     * @param int $productId
     * @return ProductInterface|null
     */
    public function getProductById(int $productId)
    {
        try {
            return $this->productRepository->getById($productId);
        } catch (\Magento\Framework\Exception\NoSuchEntityException) {
            return null;
        }
    }
}
