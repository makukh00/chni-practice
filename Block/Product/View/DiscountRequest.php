<?php

declare(strict_types=1);

namespace Makukh\PersonalDiscount\Block\Product\View;

use Magento\Catalog\Helper\Data;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

class DiscountRequest extends Template
{
    /**
     * @var Data $productHelper
     */
    private Data $productHelper;

    /**
     * @param Data $productHelper
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        Data    $productHelper,
        Context $context,
        array   $data = []
    )
    {
        parent::__construct($context, $data);
        $this->productHelper = $productHelper;
    }

    /**
     * Get cache key information incl. current product ID
     *
     * @return array
     */
    public function getCacheKeyInfo(): array
    {
        $cacheKey = parent::getCacheKeyInfo();

        if ($product = $this->productHelper->getProduct()) {
            $cacheKey['product_id'] = $product->getId();
        }

        return $cacheKey;
    }
}
