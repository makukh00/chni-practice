var config = {
    map: {
        '*': {
            'Makukh_PersonalDiscount_form': 'Makukh_PersonalDiscount/js/view/form',
            'Makukh_PersonalDiscount_formOpenButton': 'Makukh_PersonalDiscount/js/view/form-open-button',
            'Makukh_PersonalDiscount_loginButton': 'Makukh_PersonalDiscount/js/view/login-button',
            'Makukh_PersonalDiscount_submitFormAction': 'Makukh_PersonalDiscount/js/action/submit-form',
            'Makukh_PersonalDiscount_formSubmitRestrictions': 'Makukh_PersonalDiscount/js/model/form-submit-restrictions'
        }
    }
};

