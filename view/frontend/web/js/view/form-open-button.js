define([
    'jquery',
    'ko',
    'uiComponent',
    'Makukh_PersonalDiscount_formSubmitRestrictions',
    'Makukh_PersonalDiscount_form'
], function ($, ko, Component, formSubmitRestrictions) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Makukh_PersonalDiscount/form-open-button'
        },

        /**
         * Initialize data links (listens/imports/exports/links)
         * @returns {*}
         */
        initLinks: function () {
            this._super();

            // Check whether it is possible to open the modal - either form is modal or there are any other restrictions
            this.canShowOpenModalButton = ko.computed(() => {
                return this.isModal && !formSubmitRestrictions.submitDenied();
            });

            return this;
        },

        /**
         * Generate event to open the form
         */
        openRequestForm: function () {
            $(document).trigger('makukh_personal_discount_form_open');
        }
    });
});
